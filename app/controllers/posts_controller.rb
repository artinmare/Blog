class PostsController < ApplicationController
  def find
	@post = Post.find(params[:id])
  end

  def index
  	@posts = Post.all
  end

  def show
  	@find
  end

  def new
  	@post = Post.new
  end

  def edit
  	@find
  end

  def create
  	@post = Post.new(post_form)
  	
  	if @post.save
  		redirect_to @post
  	else
  		render 'new'
  	end
  end

  def update
  	@find

  	if @post.update(post_form)
  		redirect_to @post
  	else
  		render 'edit'
  	end
  end

  def destroy
  	@find
  	@post.destroy

  	redirect_to posts_path
  end

  private
  	def post_form
  		params.require(:post).permit(:title, :text)
  	end
end
